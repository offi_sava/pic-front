import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EventService {

  public checkMaster: any
  public checkWord: any
  public checkRoom: any
  private subscriber: any
  private word: any
  private room: any


  constructor() {
    console.log('IIIIIIIIIIIIINNNNNNNIIIIIIIIITTTTTTTTTT')
    this.checkMaster = new Observable(s => {
      console.log(s);
      this.subscriber = s;
    })
    this.checkMaster.subscribe()


    this.checkWord = new Observable(s => {
      console.log(s);
      this.word = s;
    })
    this.checkWord.subscribe();


    this.checkRoom = new Observable(s => {

      this.room = s;
    })
    this.checkRoom.subscribe();

  }

  public setMaster() {
    console.log(this.subscriber);
    this.subscriber.next(true);
  }

  public setSlave() {
    this.subscriber.next(false);
  }

  public setWord(word) {
    this.word.next(word);
  }

  public setRoom(roomId) {
    this.room.next(roomId);
  }
}
