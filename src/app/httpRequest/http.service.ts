import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import * as _ from 'lodash';

import { HttpHeaders, HttpParams } from '@angular/common/http';
import { catchError, retry } from 'rxjs/operators';
@Injectable()
export class HttpService {
    constructor(private http: HttpClient) { }
    configUrl = '../../assets/config.json';
    baseUrl = 'http://localhost:3001/';
    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type':  'application/json',
        }),
        responseType: 'json'
    };


    // public getRoom(url, opt) {
    //     let params = new HttpParams().set('userId', opt);
    //     console.log(params);
    //     return this.http.get(this.baseUrl + url, _.merge(this.httpOptions, {params}));
    // }

    public get(url, opts): any {
        return this.http.get(this.baseUrl + url, {params: opts});
    }


    public createRoom(url, opts): any {
        return this.http.post(this.baseUrl + url, opts);
    }

    public findRooms(url, opts): any {
        return this.http.get(this.baseUrl + url, {params: opts});
    }

    public deleteRoom(url, opts): any {
        return this.http.delete(this.baseUrl + url, {params: opts});
    }


    public put(url, opts): any {
        return this.http.put(this.baseUrl + url, opts);
    }

    public post(url, opts): any {
        return this.http.post(this.baseUrl + url, opts);
    }

    public leftRoom(url, opts): any {
        return this.http.get(this.baseUrl + url, opts);
    }
}
