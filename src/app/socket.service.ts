import { Injectable } from '@angular/core';
import {io, Socket} from 'socket.io-client';

@Injectable({
  providedIn: 'root'
})
export class SocketService {
  private socket: Socket;

  constructor() {
    this.socket = io('http://localhost:3002', {
      transports: ['websocket']
    });
  }

  emit(eventName, opts) {
    this.socket.emit(eventName, opts);
  }


  on(eventName, opts) {
    this.socket.on(eventName, opts);
  }

}
