import {Component, OnInit, ViewChild, AfterViewInit} from '@angular/core';
import * as _ from 'lodash';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Platform, ToastController } from '@ionic/angular';
import { Base64ToGallery, Base64ToGalleryOptions } from '@ionic-native/base64-to-gallery/ngx';
import { AlertController } from '@ionic/angular';
let random = () => {
  return Math.random().toString().substr(2, 8);
};

import * as $ from 'jquery';
import {SocketService} from '../socket.service';
import {EventService} from '../event.service';
import {HttpService} from '../httpRequest/http.service';
@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
// @ViewChild('canvas', { static: true })
export class Tab1Page implements OnInit {

  public isMaster = false;
  public word;
  public roomId;

  @ViewChild('imageCanvas', { static: false }) canvas: any;
  canvasElement: any;
  saveX: number;
  saveY: number;

  selectedColor = '#9e2956';
  colors = [ '#9e2956', '#c2281d', '#de722f', '#edbf4c', '#5db37e', '#459cde', '#4250ad', '#802fa3', '#X'];

  drawing = false;
  lineWidth = 5;

  constructor(private plt: Platform,
              private base64ToGallery: Base64ToGallery,
              private toastCtrl: ToastController,
              public alertController: AlertController,
              private socket: SocketService,
              private httpService: HttpService,
              private events: EventService
  ) {
  }

  async ngOnInit() {
    this.isMaster = localStorage.getItem('isMaster') === 'true';
    this.word = localStorage.getItem('secret');

    this.events.checkMaster.subscribe(isMaster => {
      this.isMaster = isMaster === true;
      let diff = this.isMaster ? 80 : 0;
      this.canvasElement.height = this.plt.height() - 162.55 - diff;
      // @ts-ignore
      localStorage.setItem('isMaster', isMaster);
      if (this.isMaster) {
        this.changeWord(false)
      }
    })

    this.events.checkWord.subscribe(word => {
      console.log(word)
      this.word = word;
      localStorage.setItem('secret', word);
    });

    this.events.checkRoom.subscribe(roomId => {
      this.roomId = roomId;
    });
    // this.socket.on('connect', () => {
    //   console.log(this.socket.id);
    // });

    // this.socket.on('disconnect', () => {
    //   console.log(this.socket.id);
    // });

    this.socket.emit('join', localStorage.getItem('roomId'));

    let _socketDraw = (ev) => {
      if (ev.roomId === localStorage.getItem('roomId')) {
        this._moved(ev);
      }
    }
    let _socketStartDrawing = (ev) =>{
      if (ev.roomId === localStorage.getItem('roomId')) {
        this._startDrawing(ev);
      }
    }
    let _socketEndDrawing = (roomId) => {
      if (roomId === localStorage.getItem('roomId')) {
        this.drawing = false;
      }
    }
    let _socketClearScreen = (roomId) => {
      if (roomId === localStorage.getItem('roomId')) {
        this.clearScreen();
      }
    }
    let _socketChangeMasterOfRoom = (opts) => {
      console.log(opts)
      if (opts.newMasterId === localStorage.getItem('userId')) {
        console.log(opts)
        // this.canvasElement.height = this.plt.height() - 162.55 - 80;
        // this.isMaster = true
        this.events.setMaster();

        this.httpService.get('catchwords', {}).subscribe(words => {
          if (words.meta.status === 'ok') {
            words = words.response;
            console.log(words)
            this.events.setWord(words[0]);
          } else {
            return this.alertController.create({
              header: 'Error',
              message: 'Problem with getting words',
              buttons: ['OK']
            }).then(alert => alert.present());
          }
        });
        // @ts-ignore
        //localStorage.setItem('isMaster', true)
      } else if (opts.roomId === localStorage.getItem('roomId')) {
        // @ts-ignore
        //localStorage.setItem('isMaster', false)
        //this.isMaster = false;
        this.events.setSlave();
        //this.canvasElement.height = this.plt.height() - 162.55;

        // let words = localStorage.getItem('catchwords');
        // if (words) {
        //   this.changeWord(true)
        //   // @ts-ignore
        //   // this.socket.emit('changedWord', {newWord: words.shift()});
        //   // localStorage.setItem ('catchwords', words);
        //   // localStorage.removeItem ('secret');
        // }
      }
    }
    let _socketChangedWord = opts => {
      if (this.isMaster) {
        localStorage.setItem('secret', opts.newWord);
        this.events.setWord(opts.newWord);
      }
    };

    this.socket.on('draw', _socketDraw);
    this.socket.on('startDrawing', _socketStartDrawing);
    this.socket.on('endDrawing', _socketEndDrawing);
    this.socket.on('clearScreen', _socketClearScreen);
    this.socket.on('changeMasterOfRoom', _socketChangeMasterOfRoom);
    this.socket.on('changedWord', _socketChangedWord);
    // this.socket.on('changeMasterOfRoom', newOts => {
    //
    // });
  //   console.log(this.plt);
    await this.presentAlert2();
  //   // Set the Canvas Element and its size
    this.canvasElement = this.canvas.nativeElement;
    this.canvasElement.width = this.plt.width();
    let diff = this.isMaster ? 80 : 0;
    this.canvasElement.height = this.plt.height() - 162.55 - diff;
  //   // this.geolocation.getCurrentPosition().then((resp) => {
  //   //   // resp.coords.latitude
  //   //   // resp.coords.longitude
  //   //   console.log('jkkjj');
  //   //   console.log(resp.coords.longitude);
  //   //   console.log(resp.coords.latitude);
  //   // }).catch((error) => {
  //   //   console.log('Error getting location', error);
  //   // });
  //
  //  let watch = this.geolocation.watchPosition();
  //   watch.subscribe((data) => {
  //     console.log(data);
  //     data can be a set of coordinates, or an error (if an error occurred).
  //     data.coords.latitude
  //     data.coords.longitude
  //  });

  }



  changeWord(emit) {

    this.httpService.get('catchwords', {}).subscribe(words => {
      if (words.meta.status === 'ok') {
        words = words.response;
        this.events.setWord(words[0]);
      } else {
        return this.alertController.create({
          header: 'Error',
          message: 'Problem with getting words',
          buttons: ['OK']
        }).then(alert => alert.present());
      }
    });
    this.socket.emit('clearScreen', localStorage.getItem('roomId'));
    this.clearScreen();
    return;

    let words = localStorage.getItem('catchwords');
    let newGame = false;
    if (!words) {
      console.log('words')
      this.httpService.get('catchwords', {}).subscribe(words => {
        if (words.meta.status === 'ok') {
          newGame = true;
          words = words.response.join();
          console.log(words)
        } else {
          return this.alertController.create({
            header: 'Error',
            message: 'Problem with getting words',
            buttons: ['OK']
          }).then(alert => alert.present());
        }
      });
    }
    // @ts-ignore
    words = words.split(',');

    console.log(words)
    // @ts-ignore
    let w = words.shift();

    console.log(w)
    //if (emit) {
      //this.socket.emit('changedWord', {newWord: w});
    //} else {
    if (emit) {
      this.events.setWord(w);
      localStorage.setItem ('secret', w);
    }
    //}

    //this.word = w;
    // @ts-ignore
    //this.socket.emit('changedWord', {newWord: w});
    localStorage.setItem('catchwords', words);
    if (newGame) {
      this.alertController.create({
        header: 'Congratulations!!!',
        message: 'New game started',
        buttons: ['OK']
      }).then(alert => alert.present());
    }

    return w;
  }

  confirmWorld() {
    this.httpService.get('rooms/' + localStorage.getItem('roomId'), {}).subscribe(data => {

      if (data.meta.status === 'ok' && _.get(data, 'response.users', []).length > 1) {
        this.events.setSlave();
        localStorage.removeItem('secret');

        this.socket.emit('changeMasterOfRoom', {
          oldMasterId: localStorage.getItem('userId'),
          roomId: localStorage.getItem('roomId')
        });
        this.socket.emit('clearScreen', localStorage.getItem('roomId'));
        this.clearScreen();
      } else {
        this.alertController.create({
          header: 'Error',
          message: _.get(data, 'response.users', []).length === 1 ? 'There is only one player in the room' : data.cause,
          buttons: ['OK']
        }).then(alert => alert.present());
      }
    });
  }


  async presentAlert2() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Please enter your name',
      message: 'This is an alert message.',
      backdropDismiss: false,
      inputs: [{name: 'name', type: 'text', placeholder: 'Please enter your name'}],
      buttons: [{
        text: 'Save',
        handler: (alertData) => {
          if (_.isEmpty(alertData.name)) {
            return false;
          }
          localStorage.setItem('userName', alertData.name);
          localStorage.setItem('userId', random());
        }
      }]
    });
    if (!localStorage.getItem('userName')) {
      await alert.present();
    }
  }

  presentAlert() {
    const alert = document.createElement('ion-alert');
    // alert.cssClass = 'my-custom-class';
    // alert.header = 'Alert';
    alert.subHeader = 'Please enter your name';
    alert.backdropDismiss = false;
    // alert.message = 'This is an alert message.';
    alert.inputs = [{ name: 'name', type: 'text', placeholder: 'Please enter your name' }];
    alert.buttons = [{
        text: 'Save',
        handler: (alertData) => {
          if (alertData === null) {
            console.log('jjj');
            return false;
          }
          if (alertData === '') {
            console.log('jkkjj');
            return false;
          }
          console.log(alertData.name);
        }
      }];
    document.body.appendChild(alert);
    return alert.present();
  }

  _startDrawing(ev) {
    this.drawing = true;
    const canvasPosition = this.canvasElement.getBoundingClientRect();
    this.saveX = ev.pageX - canvasPosition.x;
    this.saveY = ev.pageY - canvasPosition.y;
  }

  startDrawing(ev) {
    if (!this.isMaster) return;
    this._startDrawing(ev);
    // console.log('00000000000', localStorage.getItem('roomId'));
    this.socket.emit('startDrawing', _.merge(ev, {roomId: localStorage.getItem('roomId')}));
  }

  endDrawing() {
    if (!this.isMaster) return;
    this.drawing = false;
    this.socket.emit('endDrawing', localStorage.getItem('roomId'));
  }

  selectColor(color) {
    console.log(localStorage.getItem('roomId'));
    if (color === '#X') {
      this.socket.emit('clearScreen', localStorage.getItem('roomId'));
      return this.clearScreen();
    }
    this.selectedColor = color;
  }

  setBackground() {
    const background = new Image();
    background.src = './assets/code.png';
    const ctx = this.canvasElement.getContext('2d');

    background.onload = () => {
      ctx.drawImage(background, 0, 0, this.canvasElement.width, this.canvasElement.height);
    };
  }

  clearScreen() {
    const ctx = this.canvasElement.getContext('2d');
    ctx.clearRect(0, 0, this.canvasElement.width, this.canvasElement.height);
  }

  moved(ev) {
    if (!this.isMaster) return;
    if (!this.drawing) return;

    const canvasPosition = this.canvasElement.getBoundingClientRect();
    const ctx = this.canvasElement.getContext('2d');

    const currentX = (ev.pageX || ev.touches[0].pageX) - canvasPosition.x;
    const currentY = (ev.pageY || ev.touches[0].pageY) - canvasPosition.y;

    ctx.lineJoin = 'round';
    ctx.strokeStyle = this.selectedColor;
    ctx.lineWidth = this.lineWidth;
    ctx.beginPath();
    ctx.moveTo(this.saveX, this.saveY);
    ctx.lineTo(currentX, currentY);
    ctx.closePath();

    ctx.stroke();

    this.saveX = currentX;
    this.saveY = currentY;

    this.socket.emit('draw', {
      roomId: localStorage.getItem('roomId'),
      pageX: ev.pageX || ev.touches[0].pageX,
      pageY: ev.pageY || ev.touches[0].pageY,
      selectedColor: this.selectedColor,
      lineWidth: this.lineWidth
    });
  }

  _moved(ev) {
    if (!this.drawing) return;

    const canvasPosition = this.canvasElement.getBoundingClientRect();
    const ctx = this.canvasElement.getContext('2d');

    const currentX = (ev.pageX || ev.touches[0].pageX) - canvasPosition.x;
    const currentY = (ev.pageY || ev.touches[0].pageY) - canvasPosition.y;
    console.log(ev.pageX, canvasPosition.x)

    ctx.lineJoin = 'round';
    ctx.strokeStyle = ev.selectedColor;
    ctx.lineWidth = ev.lineWidth;
    console.log(this.saveX, this.saveY)
    console.log(currentX, currentY)
    ctx.beginPath();
    ctx.moveTo(this.saveX, this.saveY);
    ctx.lineTo(currentX, currentY);
    ctx.closePath();

    ctx.stroke();

    this.saveX = currentX;
    this.saveY = currentY;
  }

  exportCanvasImage() {
    const dataUrl = this.canvasElement.toDataURL();

    // Clear the current canvas
    const ctx = this.canvasElement.getContext('2d');
    ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);


    if (this.plt.is('cordova')) {
      const options: Base64ToGalleryOptions = { prefix: 'canvas_', mediaScanner:  true };

      this.base64ToGallery.base64ToGallery(dataUrl, options).then(
          async res => {
            const toast = await this.toastCtrl.create({
              message: 'Image saved to camera roll.',
              duration: 2000
            });
            toast.present();
          },
          err => console.log('Error saving image to gallery ', err)
      );
    } else {
      // Fallback for Desktop
      const data = dataUrl.split(',')[1];
      const blob = this.b64toBlob(data, 'image/png');

      const a = window.document.createElement('a');
      a.href = window.URL.createObjectURL(blob);
      a.download = 'canvasimage.png';
      document.body.appendChild(a);
      a.click();
      document.body.removeChild(a);
    }
  }

// https://forum.ionicframework.com/t/save-base64-encoded-image-to-specific-filepath/96180/3
  b64toBlob(b64Data, contentType) {
    contentType = contentType || '';
    const sliceSize = 512;
    const byteCharacters = atob(b64Data);
    const byteArrays = [];

    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);

      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      const byteArray = new Uint8Array(byteNumbers);

      byteArrays.push(byteArray);
    }

    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

}
