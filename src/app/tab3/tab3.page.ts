import {AfterViewInit, Component, OnInit} from '@angular/core';
import {XMLHttpRequest} from 'xmlhttprequest';
import {HttpService} from '../httpRequest/http.service';
import * as uuid from 'uuid';
import * as _ from 'lodash';
import {Geolocation} from '@ionic-native/geolocation/ngx';
import {AlertController} from '@ionic/angular';
import {SocketService} from '../socket.service';
import {EventService} from '../event.service';
let random = () => {
  return Math.random().toString().substr(2, 8);
};
@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page implements OnInit{

  public isMaster;
  public userName;
  public userId;
  public roomsArr = [];
  public otherRoomsArr = [];
  public roomName;
  public roomId;

  constructor(
      private httpService: HttpService, public alertController: AlertController,
      private socket: SocketService,
      private events: EventService
  ) {}


  async ngOnInit()  {
    this._setUser(
        localStorage.getItem('userName') ||  'User:' + random(),
        localStorage.getItem('userId') || random()
        );
    this._setRoom(
        localStorage.getItem('roomName') || _.get(this.roomsArr, '[0].roomName'),
        localStorage.getItem('roomId') || _.get(this.roomsArr, '[0].roomId')
    );
    if (this.userId) {
      this.findOtherRooms();
      this.findRooms();
    }

    this.socket.on('deleteRoom', () => {
      if (this.userId) {
        this.findOtherRooms();
        this.findRooms();
      }
      });
  }



  deleteRoom(roomId) {
    this.httpService.deleteRoom('rooms', {
      userId: localStorage.getItem('userId'),
      roomId: localStorage.getItem('roomId')
    }).subscribe(data => {
      if (data.meta.status === 'error') { return; }
      if (roomId === localStorage.getItem('roomId')) {
        this.events.setRoom(_.get(data, 'response[0].roomId'));
        this._setRoom(_.get(data, 'response[0].roomName'), _.get(data, 'response[0].roomId'));
      }
      this.socket.emit('deleteRoom', {roomId})
      this.roomsArr = data.response;
      localStorage.setItem('roomsArr', data.response);
    });
  }



  async createRoom() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Please enter room name',
      // subHeader: 'Please enter your name',
      // message: 'This is an alert message.',
      backdropDismiss: false,
      inputs: [{name: 'name', type: 'text', placeholder: 'room'}],
      buttons: ['Cancel', {
        text: 'Save',
        handler: (alertData) => {
          if (_.isEmpty(alertData.name)) {
            return false;
          }
          const id = Math.random().toString().substr(2, 8);
          this.httpService.post('rooms', {
            userId: this.userId, userName: this.userName, roomName: alertData.name, roomId: id
          }).subscribe(data => {
            if (data.meta.status === 'ok') {
              this.httpService.get('catchwords', {}).subscribe(words => {
                if (words.meta.status === 'ok') {
                  let word = words.response.shift();
                  localStorage.setItem ('secret', word);
                  localStorage.setItem ('catchwords', words.response);
                  this._setRoom(alertData.name, id);
                  this.roomsArr.push({roomName: alertData.name, roomId: id});
                  //localStorage.setItem ('roomsArr', this.roomsArr);
                  // @ts-ignore
                  localStorage.setItem('isMaster', true);
                  this.events.setRoom(id);
                  console.log(this.events);
                  this.events.setMaster();
                  this.events.setWord(word);
                  this.isMaster = true;
                }
              });
            } else {
              return false;
            }
          });
        }
      }]
    });
    await alert.present();
  }

  async findRoom() {
    this.httpService.findRooms('rooms', {userId: this.userId}).subscribe(data => {
      console.log('this.rooms', data);
    });
  }

  addNewUserToRoom(roomId) {
    this.httpService.put('rooms/' + roomId, {userId: this.userId, userName: this.userName}).subscribe(data => {
      if (data.meta.status === 'ok') {
        this.events.setRoom(data.response.roomId);
        this._setRoom(data.response.roomName, data.response.roomId);
        if (_.find(data.response.users, ['userId', this.userId]).isMaster || data.response.users.length === 1) {
          this.events.setMaster();
        } else {
          this.events.setSlave();
        }
      }
    });
  }

  findRooms() {
    this.httpService.findRooms('rooms', {userId: localStorage.getItem('userId')}).subscribe(data => {
      if (data.meta.status === 'ok') {
        localStorage.setItem ('roomsArr', data.response);
        this.roomsArr = data.response;
      }
    });
  }


  findOtherRooms() {
    this.httpService.get('rooms/others', {userId: localStorage.getItem('userId')}).subscribe(data => {
      if (data.meta.status === 'ok') {
        this.otherRoomsArr = data.response;
      }
    });
  }


  leftRoom(roomId) {
    this.httpService.post('rooms/' + roomId + '/left-room', {userId: this.userId}).subscribe(data => {
      if (data.meta.status === 'ok') {
        this.events.setRoom(null);
        this._setRoom(_.get(this.roomsArr, '[0].roomName'), _.get(this.roomsArr, '[0].roomId'));
      }
    });
  }

  async changeName(opts) {
    localStorage.setItem('userName', opts.target.value);
  }


  _setRoom(roomName, roomId) {
    this.roomName = roomName;
    this.roomId = roomId;
    localStorage.setItem ('roomName', roomName);
    localStorage.setItem('roomId', roomId);
  }
  _setUser(userName, userId) {
    this.userName = userName;
    this.userId = userId;
    localStorage.setItem ('userName', userName);
    localStorage.setItem('userId', userId);
  }

}
